<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payouts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Payouts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('affiliate_id');
            $table->string('referrals');
            $table->string('amount');
            $table->string('owner');
            $table->string('payout_method');
            $table->string('service_account');
            $table->string('service_id');
            $table->string('service_invoice_link');
            $table->string('description');
            $table->string('status');
            $table->string('date');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Payouts');
    }
}
