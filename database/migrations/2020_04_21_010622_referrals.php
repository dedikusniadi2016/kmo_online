<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Referrals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('referrals', function(Blueprint $table){
           $table->string('id');
           $table->string('affiliate_id');
           $table->string('rest_id');
           $table->string('customer_id');
           $table->string('parent_id');
           $table->string('description');
           $table->string('status');
           $table->string('amount');
           $table->string('currency');
           $table->string('custom');
           $table->string('context');
           $table->string('campaign');
           $table->string('type');
           $table->string('reference');
           $table->string('products');
           $table->string('payout_id');
           $table->string('date');
           $table->timestamps();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referrals');
    }
}
