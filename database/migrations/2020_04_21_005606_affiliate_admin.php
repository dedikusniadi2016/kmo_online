<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AffiliateAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate_admin', function(Blueprint $table){
        $table->increments('id');
        $table->string('rest_id');
        $table->string('affiliate_id');
        $table->string('rate');
        $table->string('rate_type');
        $table->string('flat_rate_basis');
        $table->string('payment_email');
        $table->string('status');
        $table->string('earnings');
        $table->string('unpaid_earnings');
        $table->string('referrals');
        $table->string('visits');
        $table->string('date_registered');
        $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliate_admin');
    }
}
