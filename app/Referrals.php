<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referrals extends Model
{
    protected $table = 'referrals';
    protected $fileable = [
    	'affiliate_id',
    	'rest_id',
    	'custome_id',
    	'parent_id',
    	'description',
    	'status',
    	'amount',
    	'currency',
    	'custom',
    	'context',
    	'campaign',
    	'type',
    	'reference',
    	'products',
    	'payout_id',
    	'created_at',
    	'updated_at'
	];

	public function affiliate(){
        return $this->belongsTo(affiliate::class);
    }
}
