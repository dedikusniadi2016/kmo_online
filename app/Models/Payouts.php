<?php

namespace App\Models;

use App\Models\affiliate;

use Illuminate\Database\Eloquent\Model;

class Payouts extends Model
{
    protected $table = 'Payouts';
    protected $fileable = [
    	'id',
    	'affiliate_id',
        'referrals',
        'amount',
        'owner',
        'payout_method',
        'service_account',
        'service_id',
        'service_invoice_link',
        'description',
        'status',
        'date',
    	'created_at',
    	'updated_at'
    ];

    public function users(){
        return $this->belongsTo(users::class);
    }

}
