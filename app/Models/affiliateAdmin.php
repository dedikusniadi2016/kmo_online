<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliateAdmin extends Model
{
    protected $table = 'affiliate_admin';
    protected $guarded = array();

    protected $fileable = [
    	'id',
    	'rest_id',
        'affiliate_id',
    	'rate',
    	'rate_type',
    	'flat_rate_basis',
    	'payment_email',
    	'status',
    	'earnings',
    	'unpaid_earnings',
    	'referrals',
    	'visits',
    	'date_registered',
    	'created_at',
    	'updated_at'
    ];

 public function users(){
        return $this->belongsTo(users::class);
    }

}
