<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Affiliate extends Model
{
    protected $table = 'affiliate';
    protected $fileable = [
    	'id',
    	'username',
        'password',
        'name',
        'user_registered',
        'user_activation',
        'user_status',
    	'created_at',
    	'updated_at'
    ];

  
    

}
