<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visits extends Model
{
    protected $table = 'visits';
    protected $fileable = [
    	'id',
    	'affiliate_id',
        'referrals_id',
        'rest_id',
        'url',
        'referrer',
        'campaign',
        'context',
        'ip',
    	'created_at',
    	'updated_at'
    ];    
}
