<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Models\Auth\User;
use App\Models\Category;
use App\Models\Course;
use App\Models\AffiliateAdmin;
use App\Models\Affiliate;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCoursesRequest;
use App\Http\Requests\Admin\UpdateCoursesRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;


class AffilianteController extends Controller
{
    public function index()
    {        
        $affiliate = AffiliateAdmin::select('*')
        ->join('users', 'users.id', '=', 'affiliate_admin.affiliate_id')
        ->get();

        return view('backend.affiliante.index',['affiliate' => $affiliate]);
    }

    public function active() 
    {
        $affiliate = AffiliateAdmin::select('*')
        ->join('users', 'users.id', '=', 'affiliate_admin.affiliate_id')
        ->where('affiliate_admin.status', 'active')
        ->get();

        return view('backend.affiliante.index', ['affiliate' => $affiliate]);
    }

    public function inactive() 
    {
        $affiliate = AffiliateAdmin::select('*')
        ->join('users', 'users.id', '=', 'affiliate_admin.affiliate_id')
        ->where('affiliate_admin.status', 'inactive')
        ->get();

        return view('backend.affiliante.index', ['affiliate' => $affiliate]);
    }

    public function Pending() 
    {
        $affiliate = AffiliateAdmin::select('*')
        ->join('users', 'users.id', '=', 'affiliate_admin.affiliate_id')
        ->where('affiliate_admin.status', 'Pending')
        ->get();

        return view('backend.affiliante.index', ['affiliate' => $affiliate]);
    }

    public function Rejected() 
    {
        $affiliate = AffiliateAdmin::select('*')
        ->join('users', 'users.id', '=', 'affiliate_admin.affiliate_id')
        ->where('affiliate_admin.status', 'Rejected')
        ->get();

        return view('backend.affiliante.index', ['affiliate' => $affiliate]);
    }


   public function store(Request $request)
    {
        AffiliateAdmin::create($request->all());
        return redirect()->route('admin.affiliante.index')->withFlashSuccess(trans('alerts.backend.general.success'));
    }

    public function create()
    {     
      $affiliate= User::pluck('first_name', 'id')->toArray();
      return view('backend.affiliante.create', compact('affiliate', $affiliate));
    }

    public function edit($id)
    {
        $affiliate = AffiliateAdmin::findOrFail($id);
        return view('backend.affiliante.edit',['affiliate' => $affiliate]);
    }

    public function show($id) 
    {
            $affiliate = AffiliateAdmin::findOrFail($id);
            return view('backend.affiliate.show',compact('affiliate'));
    }
    
    public function destroy($id)
    {
        AffiliateAdmin::find($id)->delete();
        return redirect()->route('admin.affiliante.index')->withFlashSuccess(trans('alerts.backend.general.deleted'));
    }

    public function status(Request $request)
    {
       
        $affiliate = AffiliateAdmin::find($request->id);
        $affiliate->status = $request->status;
        $affiliate->save();

        return response()->json(['success'=>'Status change successfully.']);
    }

}
