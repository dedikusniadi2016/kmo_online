<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Requests\Admin\StoreBundlesRequest;
use App\Http\Requests\Admin\UpdateBundlesRequest;
use App\Models\Auth\User;
use App\Models\Bundle;
use App\Models\Course;
use App\Models\CourseTimeline;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCoursesRequest;
use App\Http\Requests\Admin\UpdateCoursesRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\DataTables\Facades\DataTables;

class AffiliateUrlController extends Controller
{
    public function index()
    {
        $course = Course::pluck('title', 'title')->toArray();
        return view('backend.affiliateUrl.index', compact('course', $course));
    }

}
