<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Models\Auth\User;
use App\Models\Referrals;
use App\Models\Affiliate;
use App\Models\Course;
use App\Models\CourseTimeline;
use App\Models\Media;
use function foo\func;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCoursesRequest;
use App\Http\Requests\Admin\UpdateCoursesRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\DataTables\Facades\DataTables;

class ReferralsController extends Controller
{
    public function index() 
    {
        $referrals = Referrals::select('*')
        ->join('users', 'users.id', '=', 'referrals.affiliate_id')
        ->get();
        return view('backend.referrals.index', ['referrals' => $referrals]);
    }

    public function Paid() 
    {
        $referrals = Referrals::select('*')
        ->join('users', 'users.id', '=', 'referrals.affiliate_id')
        ->where('referrals.status', 'Paid')
        ->get();
        return view('backend.referrals.index', ['referrals' => $referrals]);
    }

    public function unpaid() 
    {
        $referrals = Referrals::select('*')
        ->join('users', 'users.id', '=', 'referrals.affiliate_id')
        ->where('referrals.status', 'unpaid')
        ->get();
        return view('backend.referrals.index', ['referrals' => $referrals]);
    }

    public function pending() 
    {
        $referrals = Referrals::select('*')
        ->join('users', 'users.id', '=', 'referrals.affiliate_id')
        ->where('referrals.status', 'pending')
        ->get();
        return view('backend.referrals.index', ['referrals' => $referrals]);
    }

    public function rejected() 
    {
        $referrals = Referrals::select('*')
        ->join('users', 'users.id', '=', 'referrals.affiliate_id')
        ->where('referrals.status', 'rejected')
        ->get();
        return view('backend.referrals.index', ['referrals' => $referrals]);
    }

    public function create() 
    {
      $referrals= User::pluck('first_name', 'id')->toArray();
      return view('backend.referrals.create', compact('referrals', $referrals));
    }

    public function edit($id)
    {
        $referrals = Referrals::findOrFail($id);
        return view('backend.referrals.edit',compact('referrals'));
    }

    public function store(Request $request)
     {
        Referrals::create($request->all());
        return redirect()->route('admin.referrals.index')->withFlashSuccess(trans('alerts.backend.general.success'));
    }

    public function update(Request $request, $id)
    {
          Referrals::find($id)->update($request->all());
          return redirect()->route('admin.referrals.index')->with('success','Post updated successfully');
    }

    public function destroy($id)
    {
        Referrals::find($id)->delete();
        return redirect()->route('admin.referrals.index')->withFlashSuccess(trans('alerts.backend.general.deleted'));
    }
}
