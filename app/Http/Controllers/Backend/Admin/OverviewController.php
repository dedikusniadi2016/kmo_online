<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Models\Course;
use App\Models\CourseTimeline;
use App\Models\Lesson;
use App\Models\Media;
use App\Models\AffiliateAdmin;

use App\Models\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreLessonsRequest;
use App\Http\Requests\Admin\UpdateLessonsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;


class OverviewController extends Controller
{
    use FileUploadTrait;

    public function index(Request $request)
    {


        $affiliate = DB::table('users as s')
        ->select('s.first_name', 's.last_name', 'ad.status','v.url','v.referrals_id','r.amount','r.description')
        ->join('affiliate_admin as ad', 's.id', '=', 'ad.affiliate_id')
        ->join('visits as v','v.affiliate_id','=','s.id')
        ->join('referrals as r','r.affiliate_id','=','s.id')
        ->get();

        return view('backend.overview.index', ['affiliate' => $affiliate]);
    }
}
