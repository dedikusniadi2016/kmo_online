<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Models\Auth\User;
use App\Models\Payouts;
use function foo\func;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCoursesRequest;
use App\Http\Requests\Admin\UpdateCoursesRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;


class PayoutsController extends Controller
{
    use FileUploadTrait;

    public function index()
    {
        $payouts = Payouts::select('*')
        ->join('users', 'users.id', '=', 'payouts.affiliate_id')
        ->get();
        return view('backend.payouts.index',['payouts' => $payouts]);
    }

    public function processing() 
    {
        $payouts = Payouts::select('*')
        ->join('users', 'users.id', '=', 'payouts.affiliate_id')
        ->where('payouts.status', 'processing')
        ->get();
        return view('backend.payouts.index', ['payouts' => $payouts]);
    }

    public function paid() 
    {
        $payouts = Payouts::select('*')
        ->join('users', 'users.id', '=', 'payouts.affiliate_id')
        ->where('payouts.status', 'paid')
        ->get();
        return view('backend.payouts.index', ['payouts' => $payouts]);
    }

    public function failled() 
    {
        $payouts = Payouts::select('*')
        ->join('users', 'users.id', '=', 'payouts.affiliate_id')
        ->where('payouts.status', 'failled')
        ->get();
        return view('backend.payouts.index', ['payouts' => $payouts]);
    }

    public function create()
    {
        return view('backend.payouts.create');
    }

    public function edit($id)
    {
        $payouts::findOrFail($id);
        return view('backend.payouts.edit',compact('payouts'));
    }

    public function show($id) 
    {
        $payouts = Payouts::with('affiliate')->find($id);
        return view('backend.payouts.show', ['payouts' => $payouts]);
    }

     public function destroy($id)
    {
        Payouts::find($id)->delete();
        return redirect()->route('admin.payouts.index')->withFlashSuccess(trans('alerts.backend.general.deleted'));
    }
 }