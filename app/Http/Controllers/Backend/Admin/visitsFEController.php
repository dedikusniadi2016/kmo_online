<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Requests\Admin\StoreBundlesRequest;
use App\Http\Requests\Admin\UpdateBundlesRequest;
use App\Models\Auth\User;
use App\Models\Bundle;
use App\Models\CourseTimeline;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCoursesRequest;
use App\Http\Requests\Admin\UpdateCoursesRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\DataTables\Facades\DataTables;

class VisitsFEController extends Controller
{
    public function index()
    {
        return view('backend.visitsFE.index');
    }

}
