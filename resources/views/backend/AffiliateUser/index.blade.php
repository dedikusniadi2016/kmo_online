@extends('backend.layouts.app')

@section('title', __('affiliate User').' | '.app_name())

@section('content')

    <div class="card">
        <div class="card-header">

                <h3 class="page-title d-inline">Affiliates User</h3>
                <div class="float-right">
                </div>

        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <div class="d-block">
                        </div>
                        <br>

                        <table id="myTable"
                               class="table table-bordered table-striped @can('category_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                            <thead>
                            <tr>

                                @can('category_delete')
                                    @if ( request('show_deleted') != 1 )
                                        <th style="text-align:center;">
                                            <input type="checkbox" class="mass" id="select-all"/>
                                        </th>
                                    @endif
                                @endcan

                                <th>Username</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>registered</th>
                                <th>status</th>
                                <th>action </th>
                            </tr>
                            </thead>
                            <tbody>
                                 @foreach ($affiliate as $data)
                                <tr>
                                    <td> </td>
                                    <td> {{$data->username}} </td>
                                    <td> {{$data->name}}</td>
                                    <td> {{$data->email}} </td>
                                    <td> {{$data->user_registered}} </td>
                                    <td> {{$data->user_status}} </td>
                                    <td> 
                                        <a href="{{ route('admin.AffiliateUser.edit',$data->id) }}">Edit</a> |
                                        <a href="{{ route('admin.AffiliateUser.show',$data->id) }}">View</a> 
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@push('after-scripts')


@endpush