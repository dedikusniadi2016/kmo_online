@extends('backend.layouts.app')

@section('title', __('report').' | '.app_name())

<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">

@section('content')

<div class="card">
  <div class="card-body">
    <h4> Export Affiliates </h4> <br>
    <label> Export affiliates to a CSV file. </label> <br>
    
<div class="row">
  <div class="col-sm-4">
    <select name="action" class="form-control" id="bulk-action-selector-top">
            <option value="all-status">All Statuses</option>
            <option value="active">Active</option>
            <option value="inactive">InActive</option>
            <option value="pending">Pending</option>
            <option value="rejected">Rejected</option>
    </select>
  </div>
</div>
<br> 
 <input type="submit" id="doaction" class="btn btn-md btn-success" value="export">   
  </div>
</div>

<div class="card">
  <div class="card-body">
    <h4> Export Referrals </h4> <br>
    <label> Export referrals to a CSV file. </label> <br>
    
<div class="row">
  <div class="col-sm-2">
    <div class="form-group"> 
      <input type="text" class="form-control" name="affiliate_name" placeholder="affiliate name">
    </div>
  </div>
  <div class="col-sm-2">
    <div class="form-group"> 
      <input type="date" class="form-control" name="from">
    </div>
  </div>
  <div class="col-sm-2">
    <div class="form-group"> 
      <input type="date" class="form-control" name="to">
    </div>
  </div>

  <div class="col-sm-2">
    <div class="form-group">
    <select name="action" class="form-control" id="bulk-action-selector-top">
            <option value="all-status">All Statuses</option>
            <option value="paid">Paid</option>
            <option value="unpaid">Unpaid</option>
            <option value="rejected">Rejected</option>
            <option value="pending">Pending</option>
    </select>

    </div>
  </div>
</div>
<label> To search for an affiliate, enter the affiliate’s login name, first name, or last name. Leave blank to export referrals for all affiliates. </label>
<br> 
 <input type="submit" id="doaction" class="btn btn-md btn-success" value="export">   
  </div>
</div>


<div class="card">
  <div class="card-body">
    <h4> Export Payouts </h4> <br>
    <label> Export payouts to a CSV file. </label> <br>
    
<div class="row">
  <div class="col-sm-2">
    <div class="form-group"> 
      <input type="text" class="form-control" name="affiliate_name" placeholder="affiliate name">
    </div>
  </div>
  <div class="col-sm-2">
    <div class="form-group"> 
      <input type="date" class="form-control" name="from">
    </div>
  </div>
  <div class="col-sm-2">
    <div class="form-group"> 
      <input type="date" class="form-control" name="to">
    </div>
  </div>

  <div class="col-sm-2">
    <div class="form-group">
    <select name="action" class="form-control" id="bulk-action-selector-top">
            <option value="processing">Processing</option>
            <option value="paid">Paid</option>
            <option value="failed">Failed</option>
    </select>

    </div>
  </div>
</div>
<label> To search for an affiliate, enter the affiliate’s login name, first name, or last name. Leave blank to export payouts for all affiliates. </label>
<br> 
 <input type="submit" id="doaction" class="btn btn-md btn-success" value="export">   
  </div>
</div>

<div class="card">
  <div class="card-body">
    <h4> Export Visits </h4> <br>
    <label> Export visits to a CSV file. </label> <br>
    
<div class="row">
  <div class="col-sm-2">
    <div class="form-group"> 
      <input type="text" class="form-control" name="affiliate_name" placeholder="affiliate name">
    </div>
  </div>
  <div class="col-sm-2">
    <div class="form-group"> 
      <input type="date" class="form-control" name="from">
    </div>
  </div>
  <div class="col-sm-2">
    <div class="form-group"> 
      <input type="date" class="form-control" name="to">
    </div>
  </div>

  <div class="col-sm-2">
    <div class="form-group">
    <select name="action" class="form-control" id="bulk-action-selector-top">
            <option value="all">All</option>
            <option value="converted">Converted</option>
            <option value="unconverted">Uncoverted</option>
    </select>

    </div>
  </div>
</div>
<label> To search for an affiliate, enter the affiliate’s login name, first name, or last name. Leave blank to export visits for all affiliates. </label>
<br> 
 <input type="submit" id="doaction" class="btn btn-md btn-success" value="export">   
  </div>
</div>


<div class="card">
  <div class="card-body">
    <h4> Export Settings </h4> <br>
<label> Export the AffiliateWP settings for this site as a .json file. This allows you to easily import the configuration into another site. </label>
<br> 
 <input type="submit" id="doaction" class="btn btn-md btn-success" value="export">   
  </div>
</div>


<div class="card">
  <div class="card-body">
    <h4> Import Settings </h4> <br>
<label> Import the AffiliateWP settings from a .json file. This file can be obtained by exporting the settings on another site using the form above. </label> <br>
<input type="file" name="import"> <br><br> 
 <input type="submit" id="doaction" class="btn btn-md btn-success" value="export">   
  </div>
</div>

<div class="card">
  <div class="card-body">
    <h4> Import Affiliates </h4> <br>
<label> Import a CSV of affiliate records. </label> <br>
<input type="file" name="import"> <br><br> 
 <input type="submit" id="doaction" class="btn btn-md btn-success" value="export csv">   
  </div>
</div>


<div class="card">
  <div class="card-body">
    <h4> Import Referrals </h4> <br>
<label> Import a CSV of referral records. </label> <br>
<input type="file" name="import"> <br><br> 
 <input type="submit" id="doaction" class="btn btn-md btn-success" value="export csv">   
  </div>
</div>

@stop

@push('after-scripts')

@endpush