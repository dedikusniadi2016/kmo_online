@extends('backend.layouts.app')

@section('title', __('affiliate').' | '.app_name())

@section('content')

    <div class="card">
        <div class="card-header">
                <h3 class="page-title d-inline">Visits</h3>
                <div class="float-right">
                       <a href="{{ route('admin.report.index') }}"
                       class="btn btn-danger">Report</a>
                </div>

        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <div class="d-block">
                            <!-- <ul class="list-inline">
                                <li class="list-inline-item">
                                    <input type="text" name="affiliate_name" placeholder="affiliate Name" class="form-control">
                                </li>
                                <li class="list-inline-item">
                                    <input type="date" name="date-now" class="form-control">
                                </li>
                                
                                <li class="list-inline-item">
                                    <input type="date" name="date-now" class="form-control">
                                </li>
                                
                                <li class="list-inline-item">
                                
                                <select name="action" id="bulk-action-selector-top" class="form-control">
                                <option value="-1"> ----</option>
                                    <option value="accept">All</option>
                                    <option value="reject">Converted</option>
                                    <option value="activate">unconverted</option>
                                </select>
                                </li>
                                
                                <li class="list-inline-item">
                                 <input type="submit" id="doaction" class="btn btn-md btn-success" value="Filter">

                                </li>
                            </ul> -->
                        </div>

                        <table id="myTable"
                               class="table table-bordered table-striped @can('category_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                            <thead>
                            <tr>

                                @can('category_delete')
                                    @if ( request('show_deleted') != 1 )
                                        <th style="text-align:center;">
                                            <input type="checkbox" class="mass" id="select-all"/>
                                        </th>
                                    @endif
                                @endcan

                                <th>Visits ID</th>
                                <th>Landing Page</th>
                                <th>Referring URl</th>
                                <th>IP</th>
                                <th>Conveted</th>
                                <th>Referral ID </th>
                                <th>Affiliate </th>
                                <th>Date </th>
                            </tr>
                            </thead>
                            <tbody>
                                  @foreach ($visits as $data)
                            <tr>
                            <td> </td>
                            <td>{{ $data->id }}</td>
                            <td>{{ $data->url }}</td>
                            <td> {{ $data->referrer }} </td>
                                    <td> {{$data->ip}} </td>
                                    <td> 1 </td>
                                    <td> {{$data->referrals_id}}</td>
                                    <td> </td>
                                    <td> {{$data->created_at}} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@push('after-scripts')


@endpush