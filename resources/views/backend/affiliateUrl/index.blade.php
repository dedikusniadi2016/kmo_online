@extends('backend.layouts.app')

@section('title', __('affiliate Url').' | '.app_name())

@section('content')

    <div class="card">
        <div class="card-header">
                <h3 class="page-title d-inline">Affiliate URL</h3>
                <div class="float-right"></div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <div class="d-block"></div>
                        <label> You referrals <b> {{ url('/') . '/course'. '/?ref=' . Auth::user()->id }} </b>  </label>
                        <br>
                        <h4> Referral URL Generator </h4>
                        <label> Enter any URL from this website in the form below to generate a referral link! </label>

<!--                         <form id="affwp-generate-ref-url" action="{{url('user/affiliateUrl')}}"  method="post">
                        <div class="form-group">
                            <label> page Urls </label>
                            <input type="text" name="affwp-url" id="affwp-url" class="form-control" 
                            value="">
                        </div>

                        < value="{{ url('/') . '/course'  . '/?ref=' . Auth::user()->id }}" -->
                        <!-- <div class="form-group">
                        <label> Class </label>
                        {{ Form::select('title',$course,null,['class' => 'form-control select2','id'=>'title']) }}
                        </div> -->

<!--                         <div class="form-group">
                            <label> Campaign </label>
                            <input type="text" name="campaign" id="affwp-campaign">
                        </div>

                        <div class="form-group">
                        @auth
                            <label> Referrals Url </label>
                            <input type="text" name="affiliate" id="affwp-referral-url" class="form-control" 
                            value="{{ url('/') . '/course'  . '/?ref=' . Auth::user()->id }}"> 
                        @endauth
                        </div>

                        <div class="form-group">
                    <input type="hidden" class="affwp-affiliate-id" value="" />
                    <input type="hidden" class="affwp-referral-var" value="" />
                        <button class="btn btn-primary" id="thisbutton"> Generate Url </button>
                        </div>
                    </form> -->


                    <form id="affwp-generate-ref-url" class="affwp-form" method="get" action="">

                        <div class="affwp-wrap affwp-base-url-wrap">
                            <label for="affwp-url"> Page URL </label>
                            <input type="text" name="url" id="affwp-url" value="{{ url('/') }}" />
                        </div>

                        <div class="affwp-wrap affwp-campaign-wrap">
                            <label for="affwp-campaign">Campaign Name (optional) </label>
                            <input type="text" name="campaign" id="affwp-campaign" value="" />
                        </div>

                        <div class="affwp-wrap affwp-referral-url-wrap">
                            <label for="affwp-referral-url">Referral URL</label>
                            <input type="text" id="affwp-referral-url" value=""/>
                            <div class="description">(now copy this referral link and share it anywhere)</div>
                        </div>

                        <div class="affwp-referral-url-submit-wrap">
                            <input type="hidden" class="affwp-affiliate-id" value="" />
                            <input type="hidden" class="affwp-referral-var" value="urldecode( affwp_get_referral_format_value() )" />
                            <input type="submit" class="button" value="Generate URL" />
                        </div>
                    </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@push('after-scripts')

<script>

$('select').on('change', function() {
  $temp = this.value;
  $("#test1").text($temp);

});


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

    $( '#affwp-generate-ref-url' ).submit( function() {

        var affwp_vars;
        var pretty_affiliate_urls = decodeURIComponent(url);

        var url                 = $( this ).find( '#affwp-url' ).val(),
            campaign            = $( this ).find( '#affwp-campaign' ).val(),
            refVar              = $( this ).find( 'input[type="hidden"].affwp-referral-var' ).val(),
            affId               = $( this ).find( 'input[type="hidden"].affwp-affiliate-id' ).val(),
            prettyAffiliateUrls = affwp_vars.pretty_affiliate_urls,
            add                 = '';

        url = url.trim();

        if ( url.indexOf( '#' ) > 0 ) {
            var fragment = url.split('#');
        }

        if ( fragment ) {
            url = fragment[0];
        }

        if ( prettyAffiliateUrls ) {
       
            if ( url.indexOf( '?' ) < 0 ) {
       
                if ( ! url.match( /\/$/ ) ) {
                    url += '/';
                }

                if( campaign.length ) {

                    campaign = '?campaign=' + campaign;

                }

            } else {

                var pieces = url.split('?');
                url = pieces[0];
                if ( ! url.match( /\/$/ ) ) {
                    url += '/';
                }
                add = '/?' + pieces[1];
                if( campaign.length ) {
                    campaign = '&campaign=' + campaign;
                }
            }

            url = url + refVar + '/' + affId + '/' + add + campaign;

            console.log(url);


            } else {

            if( campaign.length ) {

                campaign = '&campaign=' + campaign;

            }

            if ( url.indexOf( '?' ) < 0 ) {

                if ( ! url.match( /\/$/ ) ) {
                    url += '/';
                }

            } else {

                var pieces = url.split('?');
                url = pieces[0];

                if ( ! url.match( /\/$/ ) ) {
                    url += '/';
                }
                add = '&' + pieces[1];
            }

            url = url + '?' + refVar + '=' + affId + add + campaign;
            console.log(url);

        }

        if ( fragment) {
            url += '#' + fragment[1];
        }

        url = url.replace(/([^:])(\/\/+)/g, '$1/');
        url = url.replace(/ /g, '%20');

        console.log(url);

        if( affwp_is_valid_url( url ) ) {

            $( '.affwp-wrap.affwp-base-url-wrap' ).find( '.affwp-errors' ).remove();
            $( this ).find( '.affwp-referral-url-wrap' ).slideDown();
            $( this ).find( '#affwp-referral-url' ).val( url ).focus();

        } else {

            $( '.affwp-wrap.affwp-base-url-wrap' ).find( '.affwp-errors' ).remove();
            $( '.affwp-wrap.affwp-base-url-wrap' ).append( '<div class="affwp-errors"><p class="affwp-error">' + affwp_vars.invalid_url + '</p></div>' );

        }

        return false;
    });


    function affwp_is_valid_url(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

</script>


@endpush