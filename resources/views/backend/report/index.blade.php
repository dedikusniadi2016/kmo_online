@extends('backend.layouts.app')

@section('title', __('report').' | '.app_name())

<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">

@section('content')

<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" href="#referrals" role="tab" data-toggle="tab">Referrals</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#affilates" role="tab" data-toggle="tab">Affiliates</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#payouts" role="tab" data-toggle="tab">Payouts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#visits" role="tab" data-toggle="tab">Visits</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#campaigns" role="tab" data-toggle="tab">Campaigns</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div role="tabpanel" class="tab-pane fade in active" id="referrals">
<h3> Filters </h3>
<div class="float-right" style="margin-top: -20px;">
<a href="{{ route('admin.affiliante.create') }}" class="btn btn-primary">Manage Referrals</a>
</div>
<br>

  <div class="card">
  <div class="card-body">
    <div class="row">
        <div class="col-sm-2"> 
          <select name="action" class="form-control" id="bulk-action-selector-top">
                    <option value="-1">This Month</option>
                        <option value="today">Today</option>
                        <option value="yesterday">Yesterday</option>
                        <option value="this-week">this week</option>
                        <option value="last-week">last week</option>
                        <option value="last-month">last month</option>
                        <option value="this-quarter"> this quarter </option>
                        <option value="last-quarter"> last quarter </option>
                        <option value="this-year"> this year </option>
                        <option value="last-year"> Last year </option>
                        <option value="custom"> Custom </option>
            </select>
        </div>
        <div class="col-sm-2" style="margin-left: -27px;"> 
        {{ Form::select('affiliate_id',$report,null,['class' => 'form-control select2','id'=>'affiliate_id']) }}
         </div>
        <div class="col-sm-3" style="margin-left: -27px;">
            <input type="submit" id="doaction" class="btn btn-md btn-success" value="filter">
      </div>
    </div>
</div>
</div>

<h3> Quick Stats </h3>
<br>

  <div class="row">
    <div class="col-sm-4"> 
        <div class="card">
         <div class="card-header text-center">
         Paid Earnings
        </div>
      <div class="card-body">
        <h5 class="card-title text-center">No data for the current date range.</h5>
        <p class="card-text text-center">All Time</p>
      </div>
    </div>
    </div>
    <div class="col-sm-4"> 
    <div class="card">
  <div class="card-header text-center">
      Paid Earnings
  </div>
  <div class="card-body">
    <h5 class="card-title text-center">No data for the current date range.</h5>
    <p class="card-text text-center">This Month</p>
  </div>
</div>
 </div>
    <div class="col-sm-4">     
    <div class="card">
  <div class="card-header text-center">
  Paid Earnings
  </div>
  <div class="card-body">
    <h5 class="card-title text-center">$10.00</h5>
    <p class="card-text text-center">This Month</p>
  </div>
</div>
 </div>
   </div>
     <div class="row">
    <div class="col-sm-4"> 
        <div class="card">
         <div class="card-header text-center">
         Paid / Unpaid Referrals
        </div>
      <div class="card-body">
        <h5 class="card-title text-center">0 / 1</h5>
        <p class="card-text text-center">This Month</p>
      </div>
    </div>
    </div>
    <div class="col-sm-4"> 
    <div class="card">
  <div class="card-header text-center">
  Average Referral Amount
  </div>
  <div class="card-body">
    <h5 class="card-title text-center">$10.00</h5>
    <p class="card-text text-center">This Month</p>
  </div>
</div>
 </div>
</div>
  </div>
  <div role="tabpanel" class="tab-pane fade" id="affilates">

<h3> Filters </h3>
<div class="float-right" style="margin-top: -20px;">
<a href="{{ route('admin.affiliante.create') }}" class="btn btn-primary">Manage Affiliates</a>
</div>
<br>

  <div class="card">
  <div class="card-body">
    <div class="row">
        <div class="col-sm-2"> 
          <select name="action" class="form-control" id="bulk-action-selector-top">
                    <option value="-1">This Month</option>
                        <option value="today">Today</option>
                        <option value="yesterday">Yesterday</option>
                        <option value="this-week">this week</option>
                        <option value="last-week">last week</option>
                        <option value="last-month">last month</option>
                        <option value="this-quarter"> this quarter </option>
                        <option value="last-quarter"> last quarter </option>
                        <option value="this-year"> this year </option>
                        <option value="last-year"> Last year </option>
                        <option value="custom"> Custom </option>
            </select>
        </div>
        <div class="col-sm-3" style="margin-left: -27px;">
            <input type="submit" id="doaction" class="btn btn-md btn-success" value="filter">
      </div>
    </div>
</div>
</div>

<h3> Quick Stats </h3>
<br>

  <div class="row">
    <div class="col-sm-4"> 
        <div class="card">
         <div class="card-header text-center">
          Total Affiliates
        </div>
      <div class="card-body">
        <h5 class="card-title text-center">1</h5>
        <p class="card-text text-center">All Time</p>
      </div>
    </div>
    </div>
    <div class="col-sm-4"> 
    <div class="card">
  <div class="card-header text-center">
  Highest Converting Affiliate
  </div>
  <div class="card-body">
    <h5 class="card-title text-center">Affiliate #2</h5>
    <p class="card-text text-center">This Month (0 referrals)</p>
  </div>
</div>
 </div>
    <div class="col-sm-4">     
    <div class="card">
  <div class="card-header text-center">
    New Affiliates
  </div>
  <div class="card-body">
    <h5 class="card-title text-center">1</h5>
    <p class="card-text text-center">This Month</p>
  </div>
</div>
 </div>
   </div>
     <div class="row">
    <div class="col-sm-4"> 
        <div class="card">
         <div class="card-header text-center">
        Top Earning Affiliate
        </div>
      <div class="card-body">
        <h5 class="card-title text-center">No data for the current date range.</h5>
        <p class="card-text text-center">This Month</p>
      </div>
    </div>
    </div>
    </div>
  </div>
  <div role="tabpanel" class="tab-pane fade" id="payouts">
<h3> Filters</h3>
<div class="float-right" style="margin-top: -20px;">
<a href="{{ route('admin.affiliante.create') }}" class="btn btn-primary">Manage Payouts</a>
</div>
<br>
  <div class="card">
  <div class="card-body">
    <div class="row">
        <div class="col-sm-2"> 
          <select name="action" class="form-control" id="bulk-action-selector-top">
                    <option value="-1">This Month</option>
                        <option value="today">Today</option>
                        <option value="yesterday">Yesterday</option>
                        <option value="this-week">this week</option>
                        <option value="last-week">last week</option>
                        <option value="last-month">last month</option>
                        <option value="this-quarter"> this quarter </option>
                        <option value="last-quarter"> last quarter </option>
                        <option value="this-year"> this year </option>
                        <option value="last-year"> Last year </option>
                        <option value="custom"> Custom </option>
            </select>
        </div>
        <div class="col-sm-2" style="margin-left: -27px;"> 
        {{ Form::select('affiliate_id',$report,null,['class' => 'form-control select2','id'=>'affiliate_id']) }}
         </div>
        <div class="col-sm-3" style="margin-left: -27px;">
            <input type="submit" id="doaction" class="btn btn-md btn-success" value="filter">
      </div>
    </div>
</div>
</div>

<h3> Quick Stats </h3>
<br>

  <div class="row">
    <div class="col-sm-4"> 
        <div class="card">
         <div class="card-header text-center">
         Total Earnings Paid
        </div>
      <div class="card-body">
        <h5 class="card-title text-center">No data for the current date range.</h5>
        <p class="card-text text-center">All Time</p>
      </div>
    </div>
    </div>
    <div class="col-sm-4"> 
    <div class="card">
  <div class="card-header text-center">
      Total Earnings Paid
  </div>
  <div class="card-body">
    <h5 class="card-title text-center">No data for the current date range..</h5>
    <p class="card-text text-center">This Month</p>
  </div>
</div>
 </div>
    <div class="col-sm-4">     
    <div class="card">
  <div class="card-header text-center">
  Total Earnings Generated
  </div>
  <div class="card-body">
    <h5 class="card-title text-center">$10.00</h5>
    <p class="card-text text-center">This Month</p>
  </div>
</div>
 </div>
   </div>
     <div class="row">
    <div class="col-sm-4"> 
        <div class="card">
         <div class="card-header text-center">
            Total Payouts Count
        </div>
      <div class="card-body">
        <h5 class="card-title text-center">No data for the current date range. </h5>
        <p class="card-text text-center">All Time</p>
      </div>
    </div>
    </div>
  </div>
</div>
  <div role="tabpanel" class="tab-pane fade" id="visits">

<h3> Filters </h3>
<div class="float-right" style="margin-top: -20px;">
<a href="{{ route('admin.affiliante.create') }}" class="btn btn-primary">Manage Visits</a>
</div>
<br>

  <div class="card">
  <div class="card-body">
    <div class="row">
        <div class="col-sm-2"> 
          <select name="action" class="form-control" id="bulk-action-selector-top">
                    <option value="-1">This Month</option>
                        <option value="today">Today</option>
                        <option value="yesterday">Yesterday</option>
                        <option value="this-week">this week</option>
                        <option value="last-week">last week</option>
                        <option value="last-month">last month</option>
                        <option value="this-quarter"> this quarter </option>
                        <option value="last-quarter"> last quarter </option>
                        <option value="this-year"> this year </option>
                        <option value="last-year"> Last year </option>
                        <option value="custom"> Custom </option>
            </select>
        </div>
        <div class="col-sm-2" style="margin-left: -27px;"> 
          {{ Form::select('affiliate_id',$report,null,['class' => 'form-control select2','id'=>'affiliate_id']) }}   
         </div>
        <div class="col-sm-3" style="margin-left: -27px;">
            <input type="submit" id="doaction" class="btn btn-md btn-success" value="filter">
      </div>
    </div>
</div>
</div>

<h3> Quick Stats </h3>
<br>

  <div class="row">
    <div class="col-sm-4"> 
        <div class="card">
         <div class="card-header text-center">
         Total Visits
        </div>
      <div class="card-body">
        <h5 class="card-title text-center">No data for the current date range.</h5>
        <p class="card-text text-center">All Time</p>
      </div>
    </div>
    </div>
    <div class="col-sm-4"> 
    <div class="card">
  <div class="card-header text-center">
      Total Visits
  </div>
  <div class="card-body">
    <h5 class="card-title text-center">No data for the current date range..</h5>
    <p class="card-text text-center">This Month</p>
  </div>
</div>
 </div>
    <div class="col-sm-4">     
    <div class="card">
  <div class="card-header text-center">
    Successful Conversions
  </div>
  <div class="card-body">
    <h5 class="card-title text-center">No data for the current date range.</h5>
    <p class="card-text text-center">This Month</p>
  </div>
</div>
 </div>
   </div>
     <div class="row">
    <div class="col-sm-4"> 
        <div class="card">
         <div class="card-header text-center">
          Conversion Rate
        </div>
      <div class="card-body">
        <h5 class="card-title text-center">No data for the current date range. </h5>
        <p class="card-text text-center">This Month</p>
      </div>
    </div>
    </div>
    <div class="col-sm-4"> </div>
    <div class="col-sm-4"> 

<div class="card">
         <div class="card-header text-center">
          Top Referrer
        </div>
      <div class="card-body">
        <h5 class="card-title text-center">No data for the current date range. </h5>
        <p class="card-text text-center">This Month</p>
      </div>
      </div>
     </div>
  </div>
</div>
  <div role="tabpanel" class="tab-pane fade" id="campaigns">

<h3> Filters </h3>
<br>

  <div class="card">
  <div class="card-body">
    <div class="row">
        <div class="col-sm-2"> 
          <select name="action" class="form-control" id="bulk-action-selector-top">
                    <option value="-1">This Month</option>
                        <option value="today">Today</option>
                        <option value="yesterday">Yesterday</option>
                        <option value="this-week">this week</option>
                        <option value="last-week">last week</option>
                        <option value="last-month">last month</option>
                        <option value="this-quarter"> this quarter </option>
                        <option value="last-quarter"> last quarter </option>
                        <option value="this-year"> this year </option>
                        <option value="last-year"> Last year </option>
                        <option value="custom"> Custom </option>
            </select>
        </div>
        <div class="col-sm-2" style="margin-left: -27px;"> 
            {{ Form::select('affiliate_id',$report,null,['class' => 'form-control select2','id'=>'affiliate_id']) }}   
         </div>
        <div class="col-sm-3" style="margin-left: -27px;">
            <input type="submit" id="doaction" class="btn btn-md btn-success" value="filter">
      </div>
    </div>
</div>
</div>

<h3> Quick Stats </h3>
<br>

  <div class="row">
    <div class="col-sm-4"> 
        <div class="card">
         <div class="card-header text-center">
          Best Converting Campaign (All Time)
        </div>
      <div class="card-body">
        <h5 class="card-title text-center">No data for the current date range.</h5>
      </div>
    </div>
    </div>
    <div class="col-sm-4"> 
    <div class="card">
  <div class="card-header text-center">
      Most Active Campaign (This Mon
  </div>
  <div class="card-body">
    <h5 class="card-title text-center">No data for the current date range..</h5>
  </div>
</div>
 </div>
    <div class="col-sm-4">     
    <div class="card">
  <div class="card-header text-center">
    Best Converting Campaign (This Month)
  </div>
  <div class="card-body">
    <h5 class="card-title text-center">No data for the current date range.</h5>
  </div>
</div>
 </div>
   </div>
</div>
</div>
@stop

@push('after-scripts')

@endpush