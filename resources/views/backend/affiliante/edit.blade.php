@extends('backend.layouts.app')
@section('title', __('Edit Affiliate').' | '.app_name())

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="page-title d-inline">Edit Affiliate</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
              <form>
                    <div class="form-group">
                        <label for="affiliate_name">Affiliate Name</label>
                        <input type="text" class="form-control" id="affiliate_name" aria-describedby="affiliate_name" disabled
                        value="{{ old('username', $affiliate->username) }}"
                        >
                        <small> The affiliate’s first and/or last name. Will be empty if no name is specified. This can be changed on the user edit screen. </small>
                    </div>
                    
                    <div class="form-group">
                        <label for="affiliate_id">Affiliate ID</label>
                        <input type="text" name="affiliate_id" value="{{ old('id', $affiliate->id) }}" class="form-control" id="affiliate_id" disabled>
                         <small> The affiliate’s ID. This cannot be changed. </small>           
                        </div>

                        <div class="form-group">
                        <label for="affiliate_url">Affiliate URL</label>
                        <input type="text" name="affiliate_url" class="form-control" id="affiliate_url" disabled>
                         <small> The affiliate’s referral URL. This is based on global settings. </small>           
                        </div>

                         <div class="form-group">
                        <label for="user_id">User ID</label>
                        <input type="text" name="user_id" class="form-control" id="user_id" disabled>
                         <small> The affiliate’s user ID. This cannot be changed. </small>           
                        </div>

                        <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" name="username" class="form-control" id="username" disabled>
                         <small> The affiliate’s username. This cannot be changed. </small>           
                        </div>

                        <div class="form-group">
                        <label for="registration_date">Registration Date</label>
                        <input type="text" name="registration_date" class="form-control" id="registration_date" disabled>
                         <small> The affiliate’s registration date. This cannot be changed. </small>           
                        </div>

                       <div class="form-group">
                        <label for="affiliate_status">Affiliate Status</label>
                        <select name="affiliate_status" class="form-control" id="affiliate_status">
                                <option value="active">Active</option>
                                <option value="inactive">InActive</option>
                                <option value="pending">Pending</option>
                         </select>
                         <small> The status assigned to the affiliate’s account. Updating the status could trigger account related events, such as email notifications. </small>           
                        </div>


                        <div class="form-group">
                        <label for="website">Website</label>
                        <input type="text" name="website" class="form-control" id="website" disabled>
                         <small> The affiliate’s website. Will be empty if no website is specified. This can be changed on the user edit screen. </small>           
                        </div>


                    <div class="form-group">
                    <label for="exampleInputEmail1"> Referral Rate Type	 </label>  <br>   
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked>
                    <label class="form-check-label" for="inlineRadio1">Site Default</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                    <label class="form-check-label" for="inlineRadio1"> Percentage (%)  </label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                    <label class="form-check-label" for="inlineRadio1"> Flat USD  </label>
                    </div>
                    <br>
                    <small> The affiliate’s referral rate type. </small>
                    </div>

                    <div class="form-group">
                    <label for="rate"> Referral Rate </label>
                    <input type="text" name="referral_rate" id="referral_rate" class="form-control" placeholder="20">
                    <small> The affiliate’s referral rate, such as 20 for 20%. If left blank, the site default will be used. </small>
                    </div>

                      <div class="form-group">
                    <label for="account_email"> account Email </label>
                    <input type="email" name="account_email" id="account_email" class="form-control">
                    <small> The affiliate’s account email. Updating this will change the email address shown on the user’s profile page. </small>
                    </div>

                    <div class="form-group">
                    <label for="rate"> Payment Email </label>
                    <input type="email" name="payment_email" id="payment_email" class="form-control">
                    <small> Affiliate’s payment email for systems such as PayPal, Moneybookers, or others. Leave blank to use the affiliate’s user email. </small>
                    </div>

                    <div class="form-group">
                    <label for="rate"> Promotion Methods </label>
                    <textarea class="form-control" id="validationTextarea" disabled></textarea>
                    <small> Promotion methods entered by the affiliate during registration. </small>
                    </div>


                    <div class="form-group">
                    <label for="rate"> Affiliate Notes </label>
                    <textarea class="form-control" id="validationTextarea" placeholder=""></textarea>
                    <small> Enter any notes for this affiliate. Notes are only visible to the admin. </small>
                    </div>


                    <button type="submit" class="btn btn-primary">Add Affiliate</button>
                    </form>
                    

            </div>
        </div>
    </div>
@endsection

@push('after-scripts')

<script>

$(document).ready(function() {
		$('#username').keyup(function() {
			var uname = $('#username').val();
			if(uname == 0) {
				$('#result').text('');
			}
			else {
				$.ajax({
					url: '{{ route('admin.affiliante.check') }}',
					type: 'POST',
					data: 'username='+username,
					success: function(hasil) {
						if(hasil > 0) {
					     $('#result').text('Username tidak tersedia');
						}
						else {
                         $('#result').text('Username tersedia');
						}
					}
				});
			}
		});
	});

</script>

@endpush
