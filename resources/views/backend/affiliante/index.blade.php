@extends('backend.layouts.app')

@section('title', __('affiliate').' | '.app_name())


@section('content')

    <div class="card">
        <div class="card-header">

                <h3 class="page-title d-inline">Affiliates</h3>
                <div class="float-right">
                    <a href="{{ route('admin.affiliante.create') }}"
                       class="btn btn-success">Add New</a>
                       <a href="{{ route('admin.report.index') }}"
                       class="btn btn-danger">Report</a>
                </div>

        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <div class="d-block">
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.affiliante.index') }}"
                                       style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">All (0) </a>
                                </li>
                                |
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.affiliante.active') }}"
                                       style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">Active (0) </a>
                                </li>
                                |
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.affiliante.inactive') }}"
                                       style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">InActive (0)</a>
                                </li>
                                |
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.affiliante.Pending') }}"
                                       style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">Pending (0)</a>
                                </li>
                                |
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.affiliante.Rejected') }}"
                                       style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">Rejected (0)</a>
                                </li>
                            </ul>
                        </div>
                        <br>
                        <table id="myTable"
                               class="table table-bordered table-striped @can('category_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                            <thead>
                            <tr>

                                @can('category_delete')
                                    @if ( request('show_deleted') != 1 )
                                        <th style="text-align:center;">
                                            <input type="checkbox" class="mass" id="select-all"/>
                                        </th>
                                    @endif
                                @endcan

                                <th>Name</th>
                                <th>Affiliate ID</th>
                                <th>Username</th>
                                <th>Paid Earnings</th>
                                <th>Unpaid Earnings</th>
                                <th>Rate </th>
                                <th>Unpaid Referrals </th>
                                <th>Paid Referrals </th>
                                <th>Visits </th>
                                <th>Status </th>
                                <th> Action </th>
                            </tr>
                            </thead>
                            <tbody>
                                 @foreach ($affiliate as $data)
                            <tr>
                            <td> </td>
                        <td><a href="{{ route('admin.affiliante.edit',$data->id) }}">{{ $data['first_name'] }} {{$data['last_name']}}</a>
                            </td>

                            <td>{{ $data->id }}</td>
                            <td> {{ $data['first_name'] }} {{$data['last_name']}} </td>
                                    <td> Rp. {{$data->earnings}} </td>
                                    <td> Rp. {{$data->unpaid_earnings}} </td>
                                    <td> {{$data->rate}} % </td>
                                    <td> {{$data->referrals}} </td>
                                    <td> 0 </td>
                                    <td> {{$data->visits}} </td>
                                    <td> {{$data->status}} </td>
                                    <td>
                                        <a href="{{ route('admin.report.index') }}" class="btn btn-info btn-sm">Report</a>
                                        <a href="{{ route('admin.affiliante.edit',$data->id) }}" class="btn btn-secondary btn-sm">Edit</a> 
                                       

                                        <input data-id="{{$data->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="DeActive" {{ $data->status ? 'checked' : '' }}>

                                       
                                        {!! Form::open(['method' => 'DELETE','route' => ['admin.affiliante.destroy', $data->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@push('after-scripts')

<script>

$(function() {
    $('.toggle-class').change(function() {
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var id = $(this).data('id'); 
         
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "{{ route('admin.affiliante.status') }}",
            data: {'status': status, 'id': id},
            success: function(data){
              console.log(data.success)
            }
        });
    })
  })
  </script>

@endpush