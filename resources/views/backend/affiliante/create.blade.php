@extends('backend.layouts.app')
@section('title', __('New Affiliate').' | '.app_name())

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="page-title d-inline">New Affiliate</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                <div> Use this form to create a new affiliate account. Each affiliate is tied directly to a user account, so if the user account for the affiliate does not yet exist, create one. </div>
               <br>
          {!! Form::open(['route' => 'admin.affiliante.store', 'method' => 'POST']) !!}
                    <div class="form-group">
                        <label for="username">User Login Name</label>
                        {{  Form::select('affiliate_id',$affiliate,null,['class' => 'form-control select2','id'=>'affiliate_id']) }}
                    </div>
                    <div id="result"></div>

                    <div class="form-group">
                        <label for="status">Affiliate Status</label>
                        <select name="status" class="form-control" id="status">
                                <option value="active">Active</option>
                                <option value="InActive">InActive</option>
                                <option value="Pending">Pending</option>
                         </select>
                         <small> The status assigned to the affiliate’s account. </small>           
                        </div>

                    <div class="form-group">
                    <label for="rate_type"> Referral Rate Type	 </label>  <br>   
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="rate_type" id="rate_type" value="Site Default" checked>
                    <label class="form-check-label" for="rate_type">Site Default</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="rate_type" id="rate_type" value="Percentage (%)">
                    <label class="form-check-label" for="rate_type"> Percentage (%)  </label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="rate_type" id="rate_type" value="Flat USD">
                    <label class="form-check-label" for="rate_type"> Flat USD  </label>
                    </div>
                    <br>
                    <small> The affiliate’s referral rate type. </small>
                    </div>

                    <div class="form-group">
                    <label for="rate"> Referral Rate </label>
                    <input type="text" name="rate" id="rate" class="form-control" placeholder="20">
                    <small> The affiliate’s referral rate, such as 20 for 20%. If left blank, the site default will be used. </small>
                    </div>

                    <div class="form-group">
                    <label for="payment_email"> Payment Email </label>
                    <input type="email" name="payment_email" id="payment_email" class="form-control">
                    <small> Affiliate’s payment email for systems such as PayPal, Moneybookers, or others. Leave blank to use the affiliate’s user email. </small>
                    </div>

                    <div class="form-group">
                    <label for="rate"> Affiliate Notes </label>
                    <textarea class="form-control" id="validationTextarea" placeholder=""></textarea>
                    <small> Enter any notes for this affiliate. Notes are only visible to the admin. </small>
                    </div>

                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="send_email">
                        <label class="form-check-label" for="send_email"> Send welcome email after registering affiliate?</label>
                    </div>

                    <button type="submit" class="btn btn-primary">Add Affiliate</button>
                      {!! Form::close() !!}

                    

            </div>
        </div>
    </div>
@endsection

@push('after-scripts')


@endpush
