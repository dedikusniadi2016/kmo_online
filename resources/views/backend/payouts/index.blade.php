@extends('backend.layouts.app')

@section('title', __('affiliate').' | '.app_name())

@section('content')

    <div class="card">
        <div class="card-header">
                <h3 class="page-title d-inline">Payouts</h3>
                <div class="float-right">
                    <a href="{{ route('admin.report.index') }}"
                       class="btn btn-success">Report</a>
                       <a href="{{ route('admin.referrals.index') }}"
                       class="btn btn-danger">Manage Referrals</a>
                       <a href="{{ route('admin.payouts.create') }}"
                       class="btn btn-primary">Pay Affiliates</a>
                </div>

        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <div class="d-block">
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.payouts.index') }}"
                                       style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">All (0) </a>
                                </li>
                                |
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.payouts.processing') }}"
                                       style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">processing (0)</a>
                                </li>
                                |
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.payouts.paid') }}"
                                       style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">paid (0)</a>
                                </li>
                                |
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.payouts.failled') }}"
                                       style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">Failled (0)</a>
                                </li>
                            </ul>
                        </div>

                        <table id="myTable"
                               class="table table-bordered table-striped @can('category_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                            <thead>
                            <tr>

                                @can('category_delete')
                                    @if ( request('show_deleted') != 1 )
                                        <th style="text-align:center;">
                                            <input type="checkbox" class="mass" id="select-all"/>
                                        </th>
                                    @endif
                                @endcan


                                <th>Payouts ID</th>
                                <th>Amount</th>
                                <th>Affiliate</th>
                                <th>Reference</th>
                                <th>Generated By</th>
                                <th>Payout Method </th>
                                <th>Status </th>
                                <th>Date </th>
                                <th>Actions </th>
                            </tr>
                            </thead>
                            <tbody>
                                 @foreach ($payouts as $data)
                            <tr>
                            <td> </td>
                            <td>{{ $data->id }}</td>
                            <td> Rp. {{$data->amount}} </td>
                            <td> <a href="#"> {{ $data->first_name }}  </a> (ID : {{ $data->id }}) </td>
                            <td> 
                                <a href="{{ route('admin.referrals.edit',$data->id) }}">{{$data->referrals}}</a> </td>
                            <td> <a href="#"> {{ $data->first_name }} </a> (User ID : {{$data->id}}) </td>
                            <td> {{$data->payout_method}} </td>
                            <td> {{$data->status}} </td>
                            <td> {{$data->date}} </td>
                            <td>
                                <a href="{{ route('admin.payouts.show',$data->id) }}" class="btn btn-primary btn-sm">Show</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['admin.payouts.destroy', $data->id],'style'=>'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                            {!! Form::close() !!}

                             </td>

                </td>
            </tr>
            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@push('after-scripts')


@endpush