@extends('backend.layouts.app')
@section('title', __('Show payouts').' | '.app_name())

@section('content')


    <div class="card">
        <div class="card-header">
            <h3 class="page-title d-inline">Show Payouts</h3>
        </div>
        <div class="card-body">
            <div class="row" style="margin-left: 10px;">
                <form>
                 <div class="form-group">
                    <label>Affiliate : </label>
                    <label style="margin-left: 100px;"> {{ old('id', $payouts['username']) }} </label>
                </div>
                <div class="form-group">
                    <label>Affiliate : </label>
                    <label style="margin-left: 100px;"> Rp. {{ old('amount', $payouts->amount) }} </label>
                </div>
                <div class="form-group">
                    <label>Generated By : </label>
                    <label style="margin-left: 65px;"> {{ old('id', $payouts->id) }} </label>
                </div>

                <div class="form-group">
                    <label>Payout Method : </label>
                    <label style="margin-left: 52px;"> {{ old('payout_method', $payouts->payout_method) }} </label>
                </div>

                <div class="form-group">
                    <label>Payout Account : </label>
                    <label style="margin-left: 52px;"> {{ old('service_account', $payouts->service_account) }} </label>
                </div>

                <div class="form-group">
                    <label>Invoice Link  : </label>
                    <label style="margin-left: 75px;"> {{ old('service_invoice_link', $payouts->service_invoice_link) }} </label>
                </div>

                <div class="form-group">
                    <label>Status  : </label>
                    <label style="margin-left: 110px;"> {{ old('status', $payouts->status) }} </label>
                </div>

                <div class="form-group">
                    <label>Description  : </label>
                    <label style="margin-left: 75px;"> {{ old('description', $payouts->description) }} </label>
                </div>

                <div class="form-group">
                    <label> Payouts Date : </label>
                    <label style="margin-left: 65px;"> {{ old('date', $payouts->date) }} </label>
                </div>

                </form>


                 <table id="myTable"
                               class="table table-bordered table-striped @can('category_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                            <thead>
                            <tr>
                                <th>Payouts ID</th>
                                <th>Amount</th>
                                <th>Affiliate</th>
                                <th>Reference</th>
                                <th>Description</th>
                                <th>type </th>
                                <th>Date </th>
                                <th>Actions </th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td> {{$payouts->id}} </td>
                                    <td> {{$payouts->amount}} </td>
                                    <td> {{$payouts->affiliate_id}} </td>
                                    <td> {{$payouts->referrals}} </td>
                                    <td> {{$payouts->description}} </td>
                                    <td> {{$payouts->status}} </td>
                                    <td> {{$payouts->date}} </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
        </div>
    </div>
@endsection

@push('after-scripts')

@endpush
