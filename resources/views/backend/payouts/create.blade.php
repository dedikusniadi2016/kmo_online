@extends('backend.layouts.app')
@section('title', __('Pay Affiliates').' | '.app_name())

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="page-title d-inline">Pay Affiliates</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                <div> Pay your affiliates via the chosen payout method for all their unpaid referrals in the specified timeframe. </div>
               <br>
                <form>
                    <div class="form-group">
                        <label for="Affiliate">Affiliate</label>
                        <input type="text" class="form-control" id="Affiliate" aria-describedby="Affiliate">
                        <small id="Affiliate" class="form-text text-muted"> To pay a specific affiliate, enter the affiliate’s login name, first name, or last name. Leave blank to pay all affiliates.</small>
                    </div>
                    <div id="result"></div>

                    <div class="form-group">
                    <label for="start_date"> Start Date </label>
                    <input type="date" name="start_date" id="start_date" class="form-control" >
                    <small> Referrals start date.</small>
                    </div>

                    <div class="form-group">
                    <label for="end_date"> End Date </label>
                    <input type="date" name="end_date" id="end_date" class="form-control" >
                    <small> Referrals start date.</small>
                    </div>


                    <div class="form-group">
                    <label for="minimum_earnings"> Minimum Earnings	 </label>
                    <input type="text" name="minimum_earnings" id="minimum_earnings" class="form-control">
                    <small> The minimum earnings for each affiliate for a payout to be processed.</small>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1"> Payout Method</label>  <br>   
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="payouts_manual" id="payouts_manual" value="payouts_manual" checked>
                    <label class="form-check-label" for="payouts_manual"> Manual Payout </label>
                    </div>
                    <br>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="payouts_service" id="payouts_service" value="payouts_service">
                    <label class="form-check-label" for="payouts_service">  Payouts Service - Register and/or connect your account to enable this payout method  </label>
                    </div> <br>
                    <small> Choose the payout method for this payout.</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Priview Payouts</button>
                    </form>
                    

            </div>
        </div>
    </div>
@endsection

@push('after-scripts')


@endpush
