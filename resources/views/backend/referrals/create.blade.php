@extends('backend.layouts.app')
@section('title', __('New Referrals').' | '.app_name())

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="page-title d-inline">New Referrals</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                <div> Use this screen to manually create a new referral record for an affiliate.</div>
               <br>
                 {!! Form::open(['route' => 'admin.referrals.store', 'method' => 'POST']) !!}
                    <div class="form-group">
                        <label for="Affiliate">Affiliate</label>
                        {{ Form::select('affiliate_id',$referrals,null,['class' => 'form-control select2','id'=>'affiliate_id']) }}

                        <small id="Affiliate" class="form-text text-muted"> Enter the name of the affiliate or enter a partial name or email to perform a search.</small>
                    </div>
                    <div id="result"></div>

                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="text" class="form-control" id="amount" name="amount" aria-describedby="amount">
                        <small id="amount" class="form-text text-muted"> The amount of the referral, such as 15.</small>
                    </div>

                    <div class="form-group">
                        <label for="type">Referral Type</label>
                        <select name="type" class="form-control" id="type">
                                <option value="sale">Sale</option>
                                <option value="opt_in">Opt-In</option>
                                <option value="lead">Lead</option>
                         </select>
                         <small> Select the type of the referral.</small>           
                        </div>

                    <div class="form-group">
                    <label for="date"> Date </label>
                    <input type="date" name="date" id="date" class="form-control" >
                    <small> Select or enter a date for this referral.</small>
                    </div>

                    <div class="form-group">
                    <label for="description"> Description </label>
                        <textarea id="description" class="form-control" name="description" rows="3"></textarea>
                    <small> Enter a description for this referral. </small>
                    </div>

                    <div class="form-group">
                    <label for="reference"> Reference </label>
                    <input type="text" name="reference" id="reference" class="form-control">
                    <small> Enter a reference for this referral (optional). Usually this would be the transaction ID of the associated purchase.</small>
                    </div>

                    <div class="form-group">
                    <label for="context"> Context </label>
                    <input type="text" name="context" id="context" class="form-control">
                    <small> Enter a context for this referral (optional). Usually this is used to help identify the payment system that was used for the transaction.</small>
                    </div>

                    <div class="form-group">
                    <label for="custom"> Custom </label>
                    <input type="text" name="custom" id="custom" class="form-control">
                    <small> Any custom data that should be stored with the referral (optional). </small>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" class="form-control" id="status">
                                <option value="paid">paid</option>
                                <option value="unpaid">unpaid</option>
                                <option value="reject">reject</option>
                                <option value="pending">pending </option>
                         </select>
                         <small> Select the status of the referral. </small>           
                        </div>

                    <button type="submit" class="btn btn-primary">Add Referrals</button>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('after-scripts')


@endpush
