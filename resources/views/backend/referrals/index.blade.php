@extends('backend.layouts.app')

@section('title', __('affiliate').' | '.app_name())

@section('content')

    <div class="card">
        <div class="card-header">
                <h3 class="page-title d-inline">Referrals</h3>
                <div class="float-right">
                    <a href="{{ route('admin.referrals.create') }}"
                       class="btn btn-success">Add New</a>
                       <a href="{{ route('admin.report.index') }}"
                       class="btn btn-danger">Report</a>
                       <a href="{{ route('admin.payouts.create') }}"
                       class="btn btn-primary">Pay Affiliates</a>
                </div>

        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <div class="d-block">
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.referrals.index') }}"
                                       style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">All (0) </a>
                                </li>
                                |
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.referrals.Paid') }}"
                                       style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">Paid (0)</a>
                                </li>
                                |
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.referrals.unpaid') }}"
                                       style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">Unpaid (0)</a>
                                </li>
                                |
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.referrals.pending') }}"
                                       style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">Pending (0)</a>
                                </li>
                                |
                                <li class="list-inline-item">
                                    <a href="{{ route('admin.referrals.rejected') }}"
                                       style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">Rejected (0)</a>
                                </li>
                            </ul>
                        </div>
                    <!-- 
                    <div class="row">
                        <div class="col-sm-2">
                           <select name="action" class="form-control" id="bulk-action-selector-top">
                            <option value="-1">Bulk Actions</option>
                                <option value="accept">Accept</option>
                                <option value="reject">Reject</option>
                                <option value="activate">Activate</option>
                                <option value="deactivate">Deactivate</option>
                                <option value="delete">Delete</option>
                            </select>
                         </div>
                        <div class="col-sm-2" style="margin-left: -27px;">
                            <input type="submit" id="doaction" class="btn btn-md btn-success" value="Apply">
                         </div>
                         <div class="col-sm-2" style="margin-left: -125px;">
                            <input type="text" name="Affiliate-name" class="form-control" placeholder="affiliate name">
                         </div>
                         <div class="col-sm-2" style="margin-left: -27px;">
                            <input type="date" name="Affiliate-name" class="form-control">
                         </div>
                         <div class="col-sm-2" style="margin-left: -27px;">
                            <input type="date" name="Affiliate-name" class="form-control">
                         </div>
                         <div class="col-sm-2" style="margin-left: -27px;">
                           <select name="action" class="form-control" id="bulk-action-selector-top">
                            <option value="-1">All Types</option>
                                <option value="sale">Sale</option>
                                <option value="opt-ln">Opt-ln</option>
                                <option value="lead">Lead</option>
                            </select>
                         </div>

                         <div class="col-sm-2" style="margin-left: -27px;">
                            <input type="submit" id="doaction" class="btn btn-md btn-success" value="Filters">
                         </div>
                   </div> -->
                    <br>

                        <table id="myTable"
                               class="table table-bordered table-striped @can('category_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                            <thead>
                            <tr>

                                @can('category_delete')
                                    @if ( request('show_deleted') != 1 )
                                        <th style="text-align:center;">
                                            <input type="checkbox" class="mass" id="select-all"/>
                                        </th>
                                    @endif
                                @endcan

                                <th>Referral ID</th>
                                <th>Amount</th>
                                <th>Affiliate</th>
                                <th>Reference</th>
                                <th>Description</th>
                                <th>Type </th>
                                <th>Date </th>
                                <th>Actions </th>
                                <th>status </th>
                            </tr>
                            </thead>

                            <tbody>
                                @foreach ($referrals as $data)
                                <tr>
                                    <td> </td>
                                    <td>{{ $data->id}}</td>
                                    <td>Rp.{{ $data->amount}}</td>
                                    <td><a href="#">{{ $data['first_name']  }} {{ $data['last_name']  }}</a></td>
                                    <td> {{$data->reference}} </td>
                                    <td> {{$data->description}} </td>
                                    <td> {{$data->type}} </td>
                                    <td> {{$data->date}} </td>
                                    <td>
                                        <input data-id="{{$data->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Unpaid" data-off="paid" {{ $data->status ? 'checked' : '' }}>
                                        <a href="{{ route('admin.referrals.edit',$data->id) }}" class="btn btn-secondary btn-sm">Edit</a> 
                                        {!! Form::open(['method' => 'DELETE','route' => ['admin.referrals.destroy', $data->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                        {!! Form::close() !!}
                                      </td>
                                    <td> {{$data->status}} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@push('after-scripts')


@endpush