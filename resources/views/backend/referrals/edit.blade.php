@extends('backend.layouts.app')
@section('title', __('Edit Referrals').' | '.app_name())

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="page-title d-inline">Edit Referrals</h3>
        </div>
        <div class="card-body">
            <div class="row" style="margin-left: 10px;">
                <form>
                
                  {!! Form::model($referrals, ['method'=>'post','route'=>['admin.referrals.update', $referrals->id]])!!}

                 <div class="form-group">
                    <label for="referrals_id">Referrals ID : </label>
                    <input type="text" class="form-control" id="id" aria-describedby="referrals_id" value="{{ old('id', $referrals->id) }}" disabled >
                    <small id="referrals_id" class="form-text text-muted">The referral ID. This cannot be changed.</small>
                </div>

                <div class="form-group">
                    <label for="affiliate"> Affiliate : </label>
                    <label aria-describedby="affiliate"> <a href="#"> {{$referrals->affiliate_id}} </a> (ID: {{$referrals->id}} ) </label>
                   <small id="affiliate" class="form-text text-muted">The referral ID. This cannot be changed.</small>
                </div>

                 <div class="form-group">
                    <label for="payouts"> Payouts : </label>
                    <label aria-describedby="payouts"> <a href="{{ route('admin.payouts.show',$referrals->id) }}"> 
                        {{$referrals->amount}} </a> (ID: {{$referrals->id}} ) </label>
                </div>

                <div class="form-group">
                    <label for="amount">Amount : </label>
                    <input type="text" class="form-control" id="amount" aria-describedby="amount" value="{{ old('amount', $referrals->amount) }}" disabled >
                    <small id="amount" class="form-text text-muted">The referral amount cannot be changed once it has been included in a payout.</small>
                </div>

                <div class="form-group">
                    <label for="type"> Referrals Type : </label>
                    <select name="action" class="form-control" id="Referral_type">
                                <option value="sale">Sale</option>
                                <option value="opt_in">Opt-In</option>
                                <option value="lead">Lead</option>
                         </select>
                    <small class="form-text text-muted">Select the type of the referral.</small>
                </div>

                <div class="form-group">
                    <label for="date"> Date : </label>
                    <input type="text" class="form-control" id="date" aria-describedby="date" value="{{ old('date', $referrals->date) }}" disabled>
                    <small id="date" class="form-text text-muted">The date the referral was created.</small>
                </div>

                <div class="form-group">
                    <label for="visit"> Visit : </label>
                    <label> None </label>
                </div>

                <div class="form-group">
                    <label for="description"> Description : </label>
                    <textarea class="form-control" name="description" id="description"> {{ $referrals->description }}</textarea>
                    <small id="description" class="form-text text-muted">Enter a description for this referral.</small>
                </div>

                 <div class="form-group">
                    <label for="reference">Reference : </label>
                    <input type="text" class="form-control" id="reference" aria-describedby="reference" value="{{ old('reference', $referrals->reference) }}">
                    <small id="referrals_id" class="form-text text-muted">Enter a reference for this referral (optional). Usually this would be the transaction ID of the associated purchase.</small>
                </div>

                <div class="form-group">
                    <label for="context">Context : </label>
                    <input type="text" class="form-control" id="context" aria-describedby="context" value="{{ old('context', $referrals->context) }}" disabled>
                    <small id="context" class="form-text text-muted">Context for this referral (optional). Usually this is used to identify the payment system or integration that was used for the transaction.</small>
                </div>

                  <div class="form-group">
                    <label for="custom">Costom : </label>
                    <input type="text" class="form-control" id="custom" aria-describedby="custom" value="{{ old('custom', $referrals->custom) }}" disabled>
                    <small id="custom" class="form-text text-muted">Custom data stored for this referral (optional).</small>
                </div>

                <div class="form-group">
                    <label for="status"> Status : </label>
                    <select name="action" class="form-control" id="status" disabled>
                                <option value="paid">paid</option>
                                <option value="unpaid">unpaid</option>
                                <option value="reject">reject</option>
                                <option value="pending">pending </option>
                         </select>
                    <small id="status" class="form-text text-muted">The referral status cannot be changed once it has been included in a payout.</small>
                </div>

                    <button type="submit" class="btn btn-primary">Update Referrals</button>


                                      {!! Form::close() !!}

        </div>
    </div>
@endsection

@push('after-scripts')

@endpush
