@inject('request', 'Illuminate\Http\Request')
@extends('backend.layouts.app')
@section('title', __('Referral Payouts').' | '.app_name())

@section('content')

<div class="row">
    <div class="col-sm-8"> 
    <div class="card">
        <div class="card-header">
            <h4> Referral Payouts </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-6 form-group">
                </div>
            </div>
            <div class="d-block">
                    <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Payout Method</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0 </td>
                            </tbody>
                        </table>
            </div>
        </div>
    </div>
 </div>
 </div>

@stop
