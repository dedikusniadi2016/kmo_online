@extends('backend.layouts.app')

@section('title', __('Profile Settings').' | '.app_name())

@section('content')

    <div class="card">
        <div class="card-header">
                <h3 class="page-title d-inline">Profile Settings</h3>
                <div class="float-right"></div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <div class="d-block">
                        </div>
                        <form>
                        <div class="form-group">
                            <label> Your Payment Email </label>
                            <input type="text" name="email" id="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="radio" name="setting" id="Setting">
                            <label>  Enable New Referral Notifications </label>
                        </div>

                        <div class="form-group">
                        <button class="btn btn-primary"> Save Profile Setting </button>
                        </div>

                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@push('after-scripts')


@endpush