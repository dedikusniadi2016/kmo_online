@inject('request', 'Illuminate\Http\Request')
@extends('backend.layouts.app')
@section('title', __('overview').' | '.app_name())

@section('content')


<div class="row">
    <div class="col-sm-4"> 
    <div class="card">
        <div class="card-header">
            <h4> Total </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-6 form-group">
                </div>
            </div>
            <div class="d-block">
                    <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Paid earnings</th>
                                <th>Paid earnings this month</th>
                                <th>Paid earnings today</th>
                            </tr>
                            </thead>
                            <tbody>
                            <td> Rp. 0.0  </td>
                            <td> Rp. 0.0  </td>
                            <td> Rp. 0.0  </td>
                            </tbody>
                        </table>

                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Unpaid referrals </th>
                                <th>Unpaid referrals this month</th>
                                <th>Unpaid referrals today</th>
                            </tr>
                            </thead>
                            <tbody>
                            <td> 0 </td>
                            <td> 0 </td>
                            <td> 0 </td>
                            </tbody>
                        </table>

                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Unpaid earnings </th>
                                <th>Unpaid earnings this month</th>
                                <th>Unpaid earnings today </th>
                            </tr>
                            </thead>
                            <tbody>
                            <td> Rp. 0.0 </td>
                            <td> Rp. 0.0 </td>
                            <td> Rp. 0.0  </td>
                            </tbody>
                        </table>
            </div>
        </div>
    </div>
 </div>
<div class="col-sm-4">
<div class="card">
        <div class="card-header">
            <h4>Most Valuable Affiliates</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-6 form-group">
                </div>
            </div>
            <div class="d-block">
            <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Affiliate    </th>
                                <th>Earnings</th>
                                <th>Referrals </th>
                                <th> Visits </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($affiliate as $data)
                            <td> {{$data->first_name}}  </td>
                            <td> Rp 0.0 </td>
                            <td> 0 </td>
                            <td> 0 </td>
                            @endforeach
                            </tbody>
                        </table>
            </div>
        </div>
    </div>
 </div>
<div class="col-sm-4">
<div class="card">
        <div class="card-header">
            <h4>Recent Referral Visits</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-6 form-group">
                </div>
            </div>
            <div class="d-block">
            <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Affiliate</th>
                                <th>Url</th>
                                <th>Converted </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($affiliate as $data)
                            <td> {{$data->first_name}}  </td>
                            <td> {{$data->url}} </td>
                            <td> {{$data->referrals_id}} </td>
                            @endforeach
                            </tbody>
                        </table>
            </div>
        </div>
    </div>
 </div>
 </div>


 <div class="row">

<div class="col-sm-4"> </div>
<div class="col-sm-4" style="margin-top: -230px;"> 

<div class="card">
        <div class="card-header">
            <h4>Recent Referrals</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-6 form-group">
                </div>
            </div>
            <div class="d-block">
            <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Affiliate</th>
                                <th>Amount</th>
                                <th>Description </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($affiliate as $data)
                            
                            <td>{{ $data->first_name }}  </td>
                            <td> {{ $data->amount }}  </td>
                            <td> {{$data->description}}  </td>
                            @endforeach
                            </tbody>
                        </table>
            </div>
        </div>
    </div>


</div>

</div>



<div class="row">

<div class="col-sm-4">
<div class="card">
        <div class="card-header">
            <h4> Latest Affiliate Registrations </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-6 form-group">
                </div>
            </div>
            <div>

                        <table id="myTable"
                               class="table table-bordered table-striped @can('category_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                            <thead>
                            <tr>
                                <th>Affiliate</th>
                                <th>Status </th>
                                <th> Action </th>
                            </tr>
                            </thead>
                            <tbody>
                                 @foreach ($affiliate as $data)
                            <tr>
                            <td> {{ $data->first_name }} </td>
                            <td> {{$data->status}} </td>
                            <td> <a href="#"> report </a> </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
            </div>
        </div>
    </div>
 </div>
 </div>

<div class="row">
 <div class="col-sm-4">
<div class="card">
        <div class="card-header">
            <h4> Highest Converting URLs </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-6 form-group">
                </div>
            </div>
            <div class="d-block">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                     <th>URL  </th>
                     <th>Conversions</th>
                  </tr>
                </thead>
                 <tbody>
                 @foreach ($affiliate as $data)
                      <td> {{ $data->url }} </td>
                      <td> {{$data->referrals_id}} </td>
                    @endforeach
                      </tbody>
            </table>
         </div>
        </div>
    </div>
 </div>
 </div>

@stop

