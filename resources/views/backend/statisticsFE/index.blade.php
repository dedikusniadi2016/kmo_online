@inject('request', 'Illuminate\Http\Request')
@extends('backend.layouts.app')
@section('title', __('Statistics').' | '.app_name())

@section('content')

<div class="row">
    <div class="col-sm-8"> 
    <div class="card">
        <div class="card-header">
            <h4> Statistics </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-6 form-group">
                </div>
            </div>
            <div class="d-block">
                    <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Unpaid Referrals</th>
                                <th>Paid Referrals	</th>
                                <th>Visits</th>
                                <th> Conversion Rate </th>
                            </tr>
                            </thead>
                            <tbody>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0 % </td>
                            </tbody>
                        </table>

                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Unpaid Earnings </th>
                                <th>Paid Earnings</th>
                                <th>Commission Rate</th>
                            </tr>
                            </thead>
                            <tbody>
                            <td> Rp. 0.0  </td>
                            <td> Rp. 0.0  </td>
                            <td> 0 % </td>
                            </tbody>
                        </table>

                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Campaign </th>
                                <th>Visits</th>
                                <th>Unique Links </th>
                                <th> Converted </th>
                                <th> Conversion Rate </th>
                            </tr>
                            </thead>
                            <tbody>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            </tbody>
                        </table>
            </div>
        </div>
    </div>
 </div>
 </div>

@stop
