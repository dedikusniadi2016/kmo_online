@inject('request', 'Illuminate\Http\Request')
@extends('backend.layouts.app')
@section('title', __('Referrals').' | '.app_name())

@section('content')

<div class="row">
    <div class="col-sm-8"> 
    <div class="card">
        <div class="card-header">
            <h4> Referrals </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-6 form-group">
                </div>
            </div>
            <div class="d-block">
                    <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Reference</th>
                                <th>Amount</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            </tbody>
                        </table>
            </div>
        </div>
    </div>
 </div>
 </div>

@stop
